﻿using System.Collections;
using UnityEngine;

namespace Game
{
    public class FadeOutAudio : MonoBehaviour
    {
        private AudioSource _source;

        void Awake()
        {
            _source = GetComponent<AudioSource>();
        }
        
        public void Do()
        {
            StartCoroutine(Fade());
            IEnumerator Fade()
            {
                float timer = 0f;
                while (timer < 1f)
                {
                    timer += Time.deltaTime;
                    _source.volume = Mathf.Lerp(1f, 0f, timer);
                    yield return null;
                }
                Destroy(this);
            }
        }
    }
}