﻿using UnityEngine;

namespace Game
{
    public class QuitGlobal : MonoBehaviour
    {
        [RuntimeInitializeOnLoadMethod]
        static void Do()
        {
            new GameObject("QUIT HANDLER").AddComponent<QuitGlobal>();
        }

        void Start()
        {
            DontDestroyOnLoad(gameObject);
        }

        void Update()
        {
            if(Input.GetKeyDown(KeyCode.Escape))
                Application.Quit();
        }
    }
}