﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    private Player _input;
    private bool _loading;
    public CanvasGroup _canvas;
    private AudioSource[] _sources;
    private List<float> startVol = new List<float>();
    
    // Start is called before the first frame update
    void Start()
    {
        _input = ReInput.players.GetPlayer(0);
        _sources = FindObjectsOfType<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (_input.GetAnyButtonDown() && !_loading)
        {
            _loading = true;
            StartCoroutine(FadeOut());
        }
    }

    IEnumerator FadeOut()
    {
        foreach (var source in _sources)
            startVol.Add(source.volume);
        
        
        float timer = 0f;
        while (timer < 0.25f)
        {
            timer += Time.deltaTime;
            _canvas.alpha = Mathf.Lerp(0, 1, timer / 0.25f);
            for (var i = 0; i < _sources.Length; i++) 
                _sources[i].volume = Mathf.Lerp(startVol[i], 0f, timer / 0.25f);

            yield return null;
        }

        SceneManager.LoadSceneAsync(1);
    }
}
