﻿using UnityEngine;

namespace Game
{
    public class DeactivateAfterTime : MonoBehaviour
    {
        public float duration = 5f;

        private void OnEnable()
        {
            Invoke(nameof(Disable), duration);
        }

        void Disable()
        {
            gameObject.SetActive(false);
        }
    }
}