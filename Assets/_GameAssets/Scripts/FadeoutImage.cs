﻿using System.Collections;
using UnityEngine;

namespace Game
{
    public class FadeoutImage : MonoBehaviour
    {
        public float duration = 0.5f;
        private CanvasGroup _image;
        public bool fadeOnEnable = true;
        private void Awake()
        {
            _image = GetComponent<CanvasGroup>();
        }

        private void OnEnable()
        {
            if(fadeOnEnable)
                FadeOut();
        }

        public void FadeOut()
        {
            StartCoroutine(Fade());
        }

        IEnumerator Fade()
        {
            float timer = 0f;
            while (timer <duration)
            {
                timer += Time.deltaTime;
                _image.alpha = Mathf.Lerp(1, 0, timer / duration);
                yield return null;
            }
        }
    }
}