﻿using System;
using System.Collections;
using UnityEngine;

namespace Game
{
    public class FadeInImage : MonoBehaviour
    {
        public float duration = 0.5f;
        private CanvasGroup _image;
        public bool fadeOnEnable = true;
        
        private void Awake()
        {
            _image = GetComponent<CanvasGroup>();
        }

        private void OnEnable()
        {
            if (fadeOnEnable)
                FadeIn();
        }

        public void FadeIn()
        {
            StartCoroutine(Fade(null));
        }

        public void FadeInToQuit()
        {
            StartCoroutine(Fade(Application.Quit));
        }
        
        public void FadeIn(Action onComeplete)
        {
            StartCoroutine(Fade(onComeplete));
        }

        IEnumerator Fade(Action onComeplete)
        {
            float timer = 0f;
            while (timer <duration)
            {
                timer += Time.deltaTime;
                _image.alpha = Mathf.Lerp(0, 1, timer / duration);
                yield return null;
            }
            
            onComeplete?.Invoke();
        }
    }
}