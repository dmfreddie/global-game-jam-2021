﻿using System.Collections;
using System.Linq;
using UnityEngine;

namespace Game
{
    public class FadeOnCameraDistance : MonoBehaviour
    {
        private SpriteRenderer[] _renderers;
        private Transform _player;
        
        private float beginFadeDistance = 2.5f;
        bool _ending = false;
        
        void Start()
        {
            _renderers = GetComponentsInChildren<SpriteRenderer>();
            _player = GameObject.FindWithTag("Player").transform;
        }

        private void Update()
        {
            if (_ending) return;
            var distance = Vector3.Distance(transform.position, _player.position);

            float alpha = distance > beginFadeDistance ? 1f : Mathf.Lerp(1f, 0f, distance / beginFadeDistance);
            
            foreach (var spriteRenderer in _renderers)
            {
                spriteRenderer.color = new Color(1, 1, 1, alpha);
            }
        }

        public void FadeOutAndDestroy()
        {
            _ending = true;
            StartCoroutine(Do());
            
            IEnumerator Do()
            {
                float timer = 0f;
                var startColor = _renderers.First().color;
                
                while (timer < 0.25f)
                {
                    timer += Time.deltaTime;
                    foreach (var spriteRenderer in _renderers)
                        spriteRenderer.color = Color.Lerp(startColor, new Color(1, 1, 1, 0), timer / 0.25f);
                    yield return null;
                }
                
                Destroy(gameObject);
            }
        }
    }
}