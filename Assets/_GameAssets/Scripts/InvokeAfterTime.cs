﻿using UnityEngine;
using UnityEngine.Events;

namespace Game
{
    public class InvokeAfterTime : MonoBehaviour
    {
        public UnityEvent onTimeElapsed;
        public  float time;

        void OnEnable()
        {
            Invoke("Do", time);
        }

        void Do()
        {
            onTimeElapsed?.Invoke();
        }
    }
}