﻿using System;
using UnityEngine;

namespace Game
{
    public class LookAtCameraYOnly : MonoBehaviour
    {
        private Transform _target;
        private void Start()
        {
            _target = Camera.main.transform;
        }

        private void Update()
        {
            var euler = Quaternion.LookRotation((_target.position - transform.position).normalized).eulerAngles;
            var current = transform.eulerAngles;
            current.y = euler.y;
            transform.eulerAngles = current;
        }
    }
}