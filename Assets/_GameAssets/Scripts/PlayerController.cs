﻿using System;
using System.Collections.Generic;
using Cinemachine;
using Rewired;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class PlayerController : MonoBehaviour
    {
        [Header("Settings")]
        [SerializeField] 
        private float walkSpeed;
        
        [SerializeField] 
        private float strafeSpeed;

        [SerializeField] 
        private float rotationSpeed = 0.1f;

        [SerializeField] 
        private Transform cameraTransform;
        
        [SerializeField]
        private float gravity = 9.8f;
        
        private CharacterController _characterController;
        private Player _input;
        private Animator _animator;
        private Transform _transform;
        private static readonly int AnimatorSpeedProperty = Animator.StringToHash("Speed_f");
        private float _turnSmoothVelocity;

        private void OnEnable()
        {
            _transform = transform;
            _input = ReInput.players.GetPlayer(0);
            
            _characterController = GetComponent<CharacterController>();
            
            
            Destroy(GetComponent<Animator>());
            _animator = transform.GetChild(0).GetComponentInChildren<Animator>();
            _animator.SetFloat(AnimatorSpeedProperty, 0f);
        }
 

        // TODO: Make this nicer to use
        void Update()
        {
            var axis = _input.GetAxis2D("Horizontal", "Vertical");


            var targetDirection = Vector3.Scale(cameraTransform.forward, (Vector3.right) + Vector3.forward);
            if (_characterController.isGrounded && axis.y > 0)
            {
                var desiredRotationAngle = Vector3.Angle(transform.forward, targetDirection);
                var crossProduct = Vector3.Cross(_transform.forward, targetDirection).y;
                if (crossProduct < 0)
                    desiredRotationAngle *= -1;
                if (desiredRotationAngle > 10 || desiredRotationAngle < -10)
                    _transform.Rotate(Vector3.up * (desiredRotationAngle * rotationSpeed * Time.deltaTime));
            }


            var movementVector = _transform.forward * (walkSpeed * axis.y) + _transform.right * (strafeSpeed * axis.x);

            

            movementVector.y -= gravity;
            _characterController.Move(movementVector * Time.deltaTime);
            
            _animator.SetFloat(AnimatorSpeedProperty, axis.y + (axis.x / 8));
        }
    }
}