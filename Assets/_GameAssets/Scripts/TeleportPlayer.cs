﻿using System;
using UnityEngine;

namespace Game
{
    public class TeleportPlayer : MonoBehaviour
    {
        [SerializeField] private Transform _target;
        public FadeInImage fadeIn;
        public FadeoutImage fadeOut;
        public GameObject[] objectsToActivate;
        
        public void Do()
        {
            var go = Camera.main.gameObject;
            go.SetActive(false);
            GameObject.FindWithTag("Player").transform.SetPositionAndRotation(_target.position, _target.rotation);
            go.SetActive(true);
            foreach (var o in objectsToActivate) 
                o.SetActive(true);
            fadeOut.FadeOut();
        }

        public void DoWithFade()
        {
            fadeIn.FadeIn(Do);
        }
        
    }
}