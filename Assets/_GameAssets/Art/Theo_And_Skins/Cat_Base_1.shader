// Shader created with Shader Forge v1.38 
// Shader Forge (c) Freya Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:True,hqlp:True,rprd:True,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:2,rntp:3,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:2865,x:34120,y:33376,varname:node_2865,prsc:2|diff-9886-OUT,spec-358-OUT,gloss-1813-OUT,emission-7037-OUT,clip-19-OUT,voffset-6851-OUT;n:type:ShaderForge.SFN_Multiply,id:6343,x:30715,y:31814,varname:node_6343,prsc:2|A-7736-RGB,B-6665-RGB;n:type:ShaderForge.SFN_Color,id:6665,x:30461,y:31905,ptovrint:False,ptlb:Color,ptin:_Color,varname:_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5019608,c2:0.5019608,c3:0.5019608,c4:1;n:type:ShaderForge.SFN_Tex2d,id:7736,x:30461,y:31679,ptovrint:True,ptlb:Base Color,ptin:_MainTex,varname:_MainTex,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Slider,id:358,x:32701,y:32912,ptovrint:False,ptlb:Metallic,ptin:_Metallic,varname:_Metallic,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_Slider,id:1813,x:32701,y:33072,ptovrint:False,ptlb:Gloss,ptin:_Gloss,varname:_Gloss,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Color,id:6283,x:29889,y:33587,ptovrint:False,ptlb:Lazer_Colour,ptin:_Lazer_Colour,varname:_Lazer_Colour,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.5607843,c3:0.2705882,c4:1;n:type:ShaderForge.SFN_Lerp,id:5611,x:31035,y:33761,varname:node_5611,prsc:2|A-6730-OUT,B-9733-OUT,T-9872-OUT;n:type:ShaderForge.SFN_Fresnel,id:5772,x:29889,y:33795,varname:node_5772,prsc:2|EXP-9227-OUT;n:type:ShaderForge.SFN_Vector1,id:9227,x:29701,y:33810,varname:node_9227,prsc:2,v1:1;n:type:ShaderForge.SFN_Multiply,id:4141,x:30187,y:33677,varname:node_4141,prsc:2|A-6283-RGB,B-5772-OUT;n:type:ShaderForge.SFN_Vector1,id:6730,x:30187,y:33610,varname:node_6730,prsc:2,v1:0;n:type:ShaderForge.SFN_ValueProperty,id:9872,x:30394,y:34253,ptovrint:False,ptlb:Lazer,ptin:_Lazer,varname:_Lazer,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Tex2d,id:4852,x:29503,y:34754,ptovrint:False,ptlb:Noise,ptin:_Noise,varname:_Noise,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Step,id:2047,x:29770,y:34622,varname:node_2047,prsc:2|A-9854-OUT,B-4852-R;n:type:ShaderForge.SFN_ValueProperty,id:9854,x:29503,y:34560,ptovrint:False,ptlb:Alpha,ptin:_Alpha,varname:_Alpha,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:-0.1;n:type:ShaderForge.SFN_Multiply,id:2668,x:30174,y:34064,varname:node_2668,prsc:2|A-9470-OUT,B-2200-OUT;n:type:ShaderForge.SFN_Add,id:9733,x:30415,y:33802,varname:node_9733,prsc:2|A-4141-OUT,B-2668-OUT;n:type:ShaderForge.SFN_Vector1,id:9470,x:29960,y:34049,varname:node_9470,prsc:2,v1:0.3;n:type:ShaderForge.SFN_OneMinus,id:2200,x:29960,y:34109,varname:node_2200,prsc:2|IN-2047-OUT;n:type:ShaderForge.SFN_Vector1,id:8094,x:29713,y:34469,varname:node_8094,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Add,id:1274,x:29952,y:34481,varname:node_1274,prsc:2|A-8094-OUT,B-4852-R;n:type:ShaderForge.SFN_Step,id:19,x:30464,y:34603,varname:node_19,prsc:2|A-9854-OUT,B-1274-OUT;n:type:ShaderForge.SFN_Add,id:7037,x:32938,y:33243,varname:node_7037,prsc:2|A-3627-OUT,B-5611-OUT,C-1293-OUT,D-1233-OUT;n:type:ShaderForge.SFN_Fresnel,id:9044,x:30393,y:32948,varname:node_9044,prsc:2|EXP-6013-OUT;n:type:ShaderForge.SFN_Color,id:8944,x:30393,y:32740,ptovrint:False,ptlb:Electric_Color,ptin:_Electric_Color,varname:_Electric_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2705883,c2:0.6366014,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:6870,x:30886,y:32795,varname:node_6870,prsc:2|A-8944-RGB,B-9044-OUT;n:type:ShaderForge.SFN_OneMinus,id:4371,x:30674,y:33014,varname:node_4371,prsc:2|IN-9044-OUT;n:type:ShaderForge.SFN_Multiply,id:1875,x:30873,y:32986,varname:node_1875,prsc:2|A-8944-RGB,B-4371-OUT;n:type:ShaderForge.SFN_Lerp,id:5317,x:31321,y:33218,varname:node_5317,prsc:2|A-6870-OUT,B-1875-OUT,T-8234-OUT;n:type:ShaderForge.SFN_Time,id:1609,x:30447,y:33358,varname:node_1609,prsc:2;n:type:ShaderForge.SFN_Multiply,id:2836,x:30671,y:33281,varname:node_2836,prsc:2|A-4539-OUT,B-1609-T;n:type:ShaderForge.SFN_ValueProperty,id:4539,x:30444,y:33203,ptovrint:False,ptlb:Electric_Flash,ptin:_Electric_Flash,varname:_Electric_Flash,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Cos,id:1315,x:30877,y:33322,varname:node_1315,prsc:2|IN-2836-OUT;n:type:ShaderForge.SFN_RemapRange,id:8234,x:31066,y:33322,varname:node_8234,prsc:2,frmn:-1,frmx:1,tomn:0,tomx:1|IN-1315-OUT;n:type:ShaderForge.SFN_Multiply,id:3627,x:31567,y:33151,varname:node_3627,prsc:2|A-9183-OUT,B-5317-OUT;n:type:ShaderForge.SFN_ValueProperty,id:9183,x:30883,y:33157,ptovrint:False,ptlb:Electirc,ptin:_Electirc,varname:_Electirc,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Lerp,id:6335,x:31843,y:32311,varname:node_6335,prsc:2|A-1599-OUT,B-9526-OUT,T-9183-OUT;n:type:ShaderForge.SFN_Vector1,id:9526,x:31202,y:32333,varname:node_9526,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Multiply,id:8166,x:31883,y:33911,varname:node_8166,prsc:2|A-4271-RGB,B-7025-OUT;n:type:ShaderForge.SFN_ValueProperty,id:7025,x:31655,y:34069,ptovrint:False,ptlb:Blink_Boost,ptin:_Blink_Boost,varname:_Blink_Boost,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Color,id:4271,x:31387,y:33935,ptovrint:False,ptlb:Blink_Color,ptin:_Blink_Color,varname:_Blink_Color,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.2705883,c2:0.6366014,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:1293,x:32099,y:33911,varname:node_1293,prsc:2|A-8166-OUT,B-8227-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8227,x:31883,y:34093,ptovrint:False,ptlb:Blink,ptin:_Blink,varname:_Blink,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Lerp,id:9886,x:32197,y:32443,varname:node_9886,prsc:2|A-6335-OUT,B-4271-RGB,T-9183-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6013,x:30110,y:33063,ptovrint:False,ptlb:Electric_Power,ptin:_Electric_Power,varname:_Electric_Power,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:2;n:type:ShaderForge.SFN_Multiply,id:1233,x:32549,y:33315,varname:node_1233,prsc:2|A-9886-OUT,B-4009-OUT;n:type:ShaderForge.SFN_ValueProperty,id:4009,x:32356,y:33360,ptovrint:False,ptlb:Diffuse_Boost,ptin:_Diffuse_Boost,varname:_Diffuse_Boost,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.2;n:type:ShaderForge.SFN_Lerp,id:1599,x:31549,y:32135,varname:node_1599,prsc:2|A-177-OUT,B-9526-OUT,T-9872-OUT;n:type:ShaderForge.SFN_Fresnel,id:7264,x:30850,y:32215,varname:node_7264,prsc:2|EXP-4582-OUT;n:type:ShaderForge.SFN_ValueProperty,id:4582,x:30490,y:32256,ptovrint:False,ptlb:Rim_Light,ptin:_Rim_Light,varname:_Rim_Light,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:3;n:type:ShaderForge.SFN_Lerp,id:177,x:31202,y:32069,varname:node_177,prsc:2|A-6343-OUT,B-3976-OUT,T-7264-OUT;n:type:ShaderForge.SFN_Multiply,id:3976,x:30991,y:31997,varname:node_3976,prsc:2|A-6343-OUT,B-2061-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2061,x:30715,y:32052,ptovrint:False,ptlb:Rim_Boost,ptin:_Rim_Boost,varname:_Rim_Boost,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1.5;n:type:ShaderForge.SFN_NormalVector,id:4773,x:33450,y:34234,prsc:2,pt:False;n:type:ShaderForge.SFN_Multiply,id:6851,x:33918,y:34101,varname:node_6851,prsc:2|A-5589-OUT,B-4773-OUT;n:type:ShaderForge.SFN_ValueProperty,id:2216,x:32942,y:34079,ptovrint:False,ptlb:node_2216,ptin:_node_2216,varname:_node_2216,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;n:type:ShaderForge.SFN_Lerp,id:5589,x:33618,y:33921,varname:node_5589,prsc:2|A-1962-OUT,B-1195-OUT,T-43-OUT;n:type:ShaderForge.SFN_Vector1,id:1195,x:33323,y:33939,varname:node_1195,prsc:2,v1:0.05;n:type:ShaderForge.SFN_Vector1,id:1962,x:33322,y:33891,varname:node_1962,prsc:2,v1:0;n:type:ShaderForge.SFN_Relay,id:9848,x:32648,y:34522,varname:node_9848,prsc:2|IN-8212-OUT;n:type:ShaderForge.SFN_ConstantClamp,id:43,x:32888,y:34341,varname:node_43,prsc:2,min:0,max:50|IN-9848-OUT;n:type:ShaderForge.SFN_ValueProperty,id:8212,x:32212,y:34666,ptovrint:False,ptlb:Lazer_Blow,ptin:_Lazer_Blow,varname:_Lazer_Blow,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0;proporder:6665-4009-358-1813-7736-4852-9854-9872-6283-9183-8944-4539-6013-7025-8227-4271-4582-2061-2216-8212;pass:END;sub:END;*/

Shader "Shader Forge/Cat_Base_1" {
    Properties {
        _Color ("Color", Color) = (0.5019608,0.5019608,0.5019608,1)
        _Diffuse_Boost ("Diffuse_Boost", Float ) = 0.2
        _Metallic ("Metallic", Range(0, 1)) = 0
        _Gloss ("Gloss", Range(0, 1)) = 1
        _MainTex ("Base Color", 2D) = "white" {}
        _Noise ("Noise", 2D) = "black" {}
        _Alpha ("Alpha", Float ) = -0.1
        _Lazer ("Lazer", Float ) = 0
        _Lazer_Colour ("Lazer_Colour", Color) = (1,0.5607843,0.2705882,1)
        _Electirc ("Electirc", Float ) = 0
        _Electric_Color ("Electric_Color", Color) = (0.2705883,0.6366014,1,1)
        _Electric_Flash ("Electric_Flash", Float ) = 1
        _Electric_Power ("Electric_Power", Float ) = 2
        _Blink_Boost ("Blink_Boost", Float ) = 2
        _Blink ("Blink", Float ) = 0
        _Blink_Color ("Blink_Color", Color) = (0.2705883,0.6366014,1,1)
        _Rim_Light ("Rim_Light", Float ) = 3
        _Rim_Boost ("Rim_Boost", Float ) = 1.5
        _node_2216 ("node_2216", Float ) = 0
        _Lazer_Blow ("Lazer_Blow", Float ) = 0
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "Queue"="AlphaTest"
            "RenderType"="TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 d3d11_9x glcore gles gles3 vulkan metal xboxone ps4 switch
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Metallic;
            uniform float _Gloss;
            uniform float4 _Lazer_Colour;
            uniform float _Lazer;
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            uniform float _Alpha;
            uniform float4 _Electric_Color;
            uniform float _Electric_Flash;
            uniform float _Electirc;
            uniform float _Blink_Boost;
            uniform float4 _Blink_Color;
            uniform float _Blink;
            uniform float _Electric_Power;
            uniform float _Diffuse_Boost;
            uniform float _Rim_Light;
            uniform float _Rim_Boost;
            uniform float _Lazer_Blow;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
                #if defined(LIGHTMAP_ON) || defined(UNITY_SHOULD_SAMPLE_SH)
                    float4 ambientOrLightmapUV : TEXCOORD10;
                #endif
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                #ifdef LIGHTMAP_ON
                    o.ambientOrLightmapUV.xy = v.texcoord1.xy * unity_LightmapST.xy + unity_LightmapST.zw;
                    o.ambientOrLightmapUV.zw = 0;
                #endif
                #ifdef DYNAMICLIGHTMAP_ON
                    o.ambientOrLightmapUV.zw = v.texcoord2.xy * unity_DynamicLightmapST.xy + unity_DynamicLightmapST.zw;
                #endif
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float node_9848 = _Lazer_Blow;
                v.vertex.xyz += (lerp(0.0,0.05,clamp(node_9848,0,50))*v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(i.uv0, _Noise));
                clip(step(_Alpha,(0.1+_Noise_var.r)) - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = unity_4LightAtten0;
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = _Gloss;
                float perceptualRoughness = 1.0 - _Gloss;
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
/////// GI Data:
                UnityLight light;
                #ifdef LIGHTMAP_OFF
                    light.color = lightColor;
                    light.dir = lightDirection;
                    light.ndotl = LambertTerm (normalDirection, light.dir);
                #else
                    light.color = half3(0.f, 0.f, 0.f);
                    light.ndotl = 0.0f;
                    light.dir = half3(0.f, 0.f, 0.f);
                #endif
                UnityGIInput d;
                d.light = light;
                d.worldPos = i.posWorld.xyz;
                d.worldViewDir = viewDirection;
                d.atten = attenuation;
                #if defined(LIGHTMAP_ON) || defined(DYNAMICLIGHTMAP_ON)
                    d.ambient = 0;
                    d.lightmapUV = i.ambientOrLightmapUV;
                #else
                    d.ambient = i.ambientOrLightmapUV;
                #endif
                #if UNITY_SPECCUBE_BLENDING || UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMin[0] = unity_SpecCube0_BoxMin;
                    d.boxMin[1] = unity_SpecCube1_BoxMin;
                #endif
                #if UNITY_SPECCUBE_BOX_PROJECTION
                    d.boxMax[0] = unity_SpecCube0_BoxMax;
                    d.boxMax[1] = unity_SpecCube1_BoxMax;
                    d.probePosition[0] = unity_SpecCube0_ProbePosition;
                    d.probePosition[1] = unity_SpecCube1_ProbePosition;
                #endif
                d.probeHDR[0] = unity_SpecCube0_HDR;
                d.probeHDR[1] = unity_SpecCube1_HDR;
                Unity_GlossyEnvironmentData ugls_en_data;
                ugls_en_data.roughness = 1.0 - gloss;
                ugls_en_data.reflUVW = viewReflectDirection;
                UnityGI gi = UnityGlobalIllumination(d, 1, normalDirection, ugls_en_data );
                lightDirection = gi.light.dir;
                lightColor = gi.light.color;
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float3 specularColor = _Metallic;
                float specularMonochrome;
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 node_6343 = (_MainTex_var.rgb*_Color.rgb);
                float node_9526 = 0.1;
                float3 node_9886 = lerp(lerp(lerp(lerp(node_6343,(node_6343*_Rim_Boost),pow(1.0-max(0,dot(normalDirection, viewDirection)),_Rim_Light)),float3(node_9526,node_9526,node_9526),_Lazer),float3(node_9526,node_9526,node_9526),_Electirc),_Blink_Color.rgb,_Electirc);
                float3 diffuseColor = node_9886; // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                half surfaceReduction;
                #ifdef UNITY_COLORSPACE_GAMMA
                    surfaceReduction = 1.0-0.28*roughness*perceptualRoughness;
                #else
                    surfaceReduction = 1.0/(roughness*roughness + 1.0);
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                half grazingTerm = saturate( gloss + specularMonochrome );
                float3 indirectSpecular = (gi.indirect.specular);
                indirectSpecular *= FresnelLerp (specularColor, grazingTerm, NdotV);
                indirectSpecular *= surfaceReduction;
                float3 specular = (directSpecular + indirectSpecular);
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += gi.indirect.diffuse;
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float node_9044 = pow(1.0-max(0,dot(normalDirection, viewDirection)),_Electric_Power);
                float4 node_1609 = _Time;
                float node_6730 = 0.0;
                float3 emissive = ((_Electirc*lerp((_Electric_Color.rgb*node_9044),(_Electric_Color.rgb*(1.0 - node_9044)),(cos((_Electric_Flash*node_1609.g))*0.5+0.5)))+lerp(float3(node_6730,node_6730,node_6730),((_Lazer_Colour.rgb*pow(1.0-max(0,dot(normalDirection, viewDirection)),1.0))+(0.3*(1.0 - step(_Alpha,_Noise_var.r)))),_Lazer)+((_Blink_Color.rgb*_Blink_Boost)*_Blink)+(node_9886*_Diffuse_Boost));
/// Final Color:
                float3 finalColor = diffuse + specular + emissive;
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 d3d11_9x glcore gles gles3 vulkan metal xboxone ps4 switch
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Metallic;
            uniform float _Gloss;
            uniform float4 _Lazer_Colour;
            uniform float _Lazer;
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            uniform float _Alpha;
            uniform float4 _Electric_Color;
            uniform float _Electric_Flash;
            uniform float _Electirc;
            uniform float _Blink_Boost;
            uniform float4 _Blink_Color;
            uniform float _Blink;
            uniform float _Electric_Power;
            uniform float _Diffuse_Boost;
            uniform float _Rim_Light;
            uniform float _Rim_Boost;
            uniform float _Lazer_Blow;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 tangent : TANGENT;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
                float3 tangentDir : TEXCOORD5;
                float3 bitangentDir : TEXCOORD6;
                LIGHTING_COORDS(7,8)
                UNITY_FOG_COORDS(9)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.tangentDir = normalize( mul( unity_ObjectToWorld, float4( v.tangent.xyz, 0.0 ) ).xyz );
                o.bitangentDir = normalize(cross(o.normalDir, o.tangentDir) * v.tangent.w);
                float node_9848 = _Lazer_Blow;
                v.vertex.xyz += (lerp(0.0,0.05,clamp(node_9848,0,50))*v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3x3 tangentTransform = float3x3( i.tangentDir, i.bitangentDir, i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(i.uv0, _Noise));
                clip(step(_Alpha,(0.1+_Noise_var.r)) - 0.5);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = unity_4LightAtten0;
                float3 attenColor = attenuation * _LightColor0.xyz;
                float Pi = 3.141592654;
                float InvPi = 0.31830988618;
///////// Gloss:
                float gloss = _Gloss;
                float perceptualRoughness = 1.0 - _Gloss;
                float roughness = perceptualRoughness * perceptualRoughness;
                float specPow = exp2( gloss * 10.0 + 1.0 );
////// Specular:
                float NdotL = saturate(dot( normalDirection, lightDirection ));
                float LdotH = saturate(dot(lightDirection, halfDirection));
                float3 specularColor = _Metallic;
                float specularMonochrome;
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 node_6343 = (_MainTex_var.rgb*_Color.rgb);
                float node_9526 = 0.1;
                float3 node_9886 = lerp(lerp(lerp(lerp(node_6343,(node_6343*_Rim_Boost),pow(1.0-max(0,dot(normalDirection, viewDirection)),_Rim_Light)),float3(node_9526,node_9526,node_9526),_Lazer),float3(node_9526,node_9526,node_9526),_Electirc),_Blink_Color.rgb,_Electirc);
                float3 diffuseColor = node_9886; // Need this for specular when using metallic
                diffuseColor = DiffuseAndSpecularFromMetallic( diffuseColor, specularColor, specularColor, specularMonochrome );
                specularMonochrome = 1.0-specularMonochrome;
                float NdotV = abs(dot( normalDirection, viewDirection ));
                float NdotH = saturate(dot( normalDirection, halfDirection ));
                float VdotH = saturate(dot( viewDirection, halfDirection ));
                float visTerm = SmithJointGGXVisibilityTerm( NdotL, NdotV, roughness );
                float normTerm = GGXTerm(NdotH, roughness);
                float specularPBL = (visTerm*normTerm) * UNITY_PI;
                #ifdef UNITY_COLORSPACE_GAMMA
                    specularPBL = sqrt(max(1e-4h, specularPBL));
                #endif
                specularPBL = max(0, specularPBL * NdotL);
                #if defined(_SPECULARHIGHLIGHTS_OFF)
                    specularPBL = 0.0;
                #endif
                specularPBL *= any(specularColor) ? 1.0 : 0.0;
                float3 directSpecular = attenColor*specularPBL*FresnelTerm(specularColor, LdotH);
                float3 specular = directSpecular;
/////// Diffuse:
                NdotL = max(0.0,dot( normalDirection, lightDirection ));
                half fd90 = 0.5 + 2 * LdotH * LdotH * (1-gloss);
                float nlPow5 = Pow5(1-NdotL);
                float nvPow5 = Pow5(1-NdotV);
                float3 directDiffuse = ((1 +(fd90 - 1)*nlPow5) * (1 + (fd90 - 1)*nvPow5) * NdotL) * attenColor;
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse + specular;
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Back
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 d3d11_9x glcore gles gles3 vulkan metal xboxone ps4 switch
            #pragma target 3.0
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            uniform float _Alpha;
            uniform float _Lazer_Blow;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
                float2 uv1 : TEXCOORD2;
                float2 uv2 : TEXCOORD3;
                float4 posWorld : TEXCOORD4;
                float3 normalDir : TEXCOORD5;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float node_9848 = _Lazer_Blow;
                v.vertex.xyz += (lerp(0.0,0.05,clamp(node_9848,0,50))*v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(i.uv0, _Noise));
                clip(step(_Alpha,(0.1+_Noise_var.r)) - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
        Pass {
            Name "Meta"
            Tags {
                "LightMode"="Meta"
            }
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_META 1
            #define SHOULD_SAMPLE_SH ( defined (LIGHTMAP_OFF) && defined(DYNAMICLIGHTMAP_OFF) )
            #define _GLOSSYENV 1
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #include "UnityMetaPass.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile LIGHTMAP_OFF LIGHTMAP_ON
            #pragma multi_compile DIRLIGHTMAP_OFF DIRLIGHTMAP_COMBINED DIRLIGHTMAP_SEPARATE
            #pragma multi_compile DYNAMICLIGHTMAP_OFF DYNAMICLIGHTMAP_ON
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 d3d11_9x glcore gles gles3 vulkan metal xboxone ps4 switch
            #pragma target 3.0
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _Metallic;
            uniform float _Gloss;
            uniform float4 _Lazer_Colour;
            uniform float _Lazer;
            uniform sampler2D _Noise; uniform float4 _Noise_ST;
            uniform float _Alpha;
            uniform float4 _Electric_Color;
            uniform float _Electric_Flash;
            uniform float _Electirc;
            uniform float _Blink_Boost;
            uniform float4 _Blink_Color;
            uniform float _Blink;
            uniform float _Electric_Power;
            uniform float _Diffuse_Boost;
            uniform float _Rim_Light;
            uniform float _Rim_Boost;
            uniform float _Lazer_Blow;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                float2 texcoord2 : TEXCOORD2;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
                float2 uv2 : TEXCOORD2;
                float4 posWorld : TEXCOORD3;
                float3 normalDir : TEXCOORD4;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                o.uv2 = v.texcoord2;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                float node_9848 = _Lazer_Blow;
                v.vertex.xyz += (lerp(0.0,0.05,clamp(node_9848,0,50))*v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityMetaVertexPosition(v.vertex, v.texcoord1.xy, v.texcoord2.xy, unity_LightmapST, unity_DynamicLightmapST );
                return o;
            }
            float4 frag(VertexOutput i) : SV_Target {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                UnityMetaInput o;
                UNITY_INITIALIZE_OUTPUT( UnityMetaInput, o );
                
                float node_9044 = pow(1.0-max(0,dot(normalDirection, viewDirection)),_Electric_Power);
                float4 node_1609 = _Time;
                float node_6730 = 0.0;
                float4 _Noise_var = tex2D(_Noise,TRANSFORM_TEX(i.uv0, _Noise));
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
                float3 node_6343 = (_MainTex_var.rgb*_Color.rgb);
                float node_9526 = 0.1;
                float3 node_9886 = lerp(lerp(lerp(lerp(node_6343,(node_6343*_Rim_Boost),pow(1.0-max(0,dot(normalDirection, viewDirection)),_Rim_Light)),float3(node_9526,node_9526,node_9526),_Lazer),float3(node_9526,node_9526,node_9526),_Electirc),_Blink_Color.rgb,_Electirc);
                o.Emission = ((_Electirc*lerp((_Electric_Color.rgb*node_9044),(_Electric_Color.rgb*(1.0 - node_9044)),(cos((_Electric_Flash*node_1609.g))*0.5+0.5)))+lerp(float3(node_6730,node_6730,node_6730),((_Lazer_Colour.rgb*pow(1.0-max(0,dot(normalDirection, viewDirection)),1.0))+(0.3*(1.0 - step(_Alpha,_Noise_var.r)))),_Lazer)+((_Blink_Color.rgb*_Blink_Boost)*_Blink)+(node_9886*_Diffuse_Boost));
                
                float3 diffColor = node_9886;
                float specularMonochrome;
                float3 specColor;
                diffColor = DiffuseAndSpecularFromMetallic( diffColor, _Metallic, specColor, specularMonochrome );
                float roughness = 1.0 - _Gloss;
                o.Albedo = diffColor + specColor * roughness * roughness * 0.5;
                
                return UnityMetaFragment( o );
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
